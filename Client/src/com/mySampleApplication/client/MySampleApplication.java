package com.mySampleApplication.client;

import com.blogspot.ctasada.gwt.eureka.client.ui.TimeBox;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.DOM;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.Date;

/**
 * Entry point classes define <code>onModuleLoad()</code>
 */
public class MySampleApplication implements EntryPoint {

    private TextBox fioFailed = new TextBox();
    private TextBox date1 = new TextBox();
    private TextBox date2 = new TextBox();
    private TextBox date3 = new TextBox();
    private TextBox date4 = new TextBox();
    private Button enterButton = new Button("Enter");
    private Button showAllButton = new Button("Show All");
    private HorizontalPanel horizontalPanel = new HorizontalPanel();
    private VerticalPanel verticalPanel = new VerticalPanel();

    @Override
    public void onModuleLoad() {

        date1.setMaxLength(2);
        date2.setMaxLength(2);
        date3.setMaxLength(2); //for time format 00:00
        date4.setMaxLength(2);

        date1.setPixelSize(35,20);
        date2.setPixelSize(35,20);
        date3.setPixelSize(35,20);
        date4.setPixelSize(35,20);

        horizontalPanel.add(date1);
        horizontalPanel.add(new Label(":"));
        horizontalPanel.add(date2);
        horizontalPanel.add(new Label("-"));
        horizontalPanel.add(date3);
        horizontalPanel.add(new Label(":"));
        horizontalPanel.add(date4);

        verticalPanel.add(enterButton);
        verticalPanel.add(showAllButton);

        RootPanel.get("enterFio").add(fioFailed);
        RootPanel.get("enterTheTime").add(horizontalPanel);
        RootPanel.get("verticalPanel").add(verticalPanel);

        fioFailed.setFocus(true);


        CellTable<Integer> table = new CellTable<Integer>();
        table.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.ENABLED);

        TextColumn<Integer> nameColumn =
    }
}