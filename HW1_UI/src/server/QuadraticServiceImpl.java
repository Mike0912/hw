package server;

import client.QuadraticServiceIntf;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import java.util.LinkedHashSet;
import java.util.Set;

public class QuadraticServiceImpl extends RemoteServiceServlet implements QuadraticServiceIntf{

    @Override
    public Set<Double> rezultQuadratic(double a, double b, double c) throws IllegalArgumentException {
        Set<Double> integerSet = new LinkedHashSet<>();
        double d = b*b - 4*a*c;
        if (d >= 0){
            double x = Math.round((-b - Math.sqrt(d))/2*a);
            integerSet.add(x);
            x = Math.round((-b + Math.sqrt(d))/2*a);
            integerSet.add(x);
        }
        return integerSet;
    }
}
