package client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.Set;

@RemoteServiceRelativePath("QuadraticService")
public interface QuadraticServiceIntf extends RemoteService{
    Set<Double> rezultQuadratic(double a, double b, double c) throws IllegalArgumentException;
}
