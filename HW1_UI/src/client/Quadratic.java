package client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.Set;

public class Quadratic implements EntryPoint {

    private TextBox slot1 = new TextBox();
    private TextBox slot2 = new TextBox();
    private TextBox slot3 = new TextBox();
    private Button buttonGo = new Button();
    private TextBox textBoxRez = new TextBox();
    private Label labelError = new Label();
    private double a,b,c;
    private final QuadraticServiceIntfAsync quadraticService = GWT.create(QuadraticServiceIntf.class);

    public void onModuleLoad() {

        slot1.setMaxLength(3);
        slot1.setPixelSize(40, 20);
        slot2.setMaxLength(3);
        slot2.setPixelSize(40, 20);
        slot3.setMaxLength(3);
        slot3.setPixelSize(40, 20);
        buttonGo.setPixelSize(50, 30);
        buttonGo.setText("Go");
        textBoxRez.setReadOnly(true);
        textBoxRez.setVisible(false);
        labelError.setVisible(false);

        RootPanel.get("slot1").add(slot1);
        RootPanel.get("slot2").add(slot2);
        RootPanel.get("slot3").add(slot3);
        RootPanel.get("buttonGo").add(buttonGo);
        RootPanel.get("textBoxRez").add(textBoxRez);
        RootPanel.get("labelError").add(labelError);

        /*buttonGo.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {

                *//*if     ((validDate(new String(slot1.getText()))) &&
                        (validDate(new String(slot2.getText()))) &&
                        (validDate(new String(slot3.getText())))){*//*

                    a = Double.parseDouble(slot1.getValue());
                    b = Double.parseDouble(slot2.getValue());
                    c = Double.parseDouble(slot3.getValue());

                    Window.alert(slot1.getValue());
             //       labelError.setVisible(false);
               //     sendToServer();

                *//*}else{

                    labelError.setText("Введите данные еще раз");
                    labelError.setVisible(true);
                    return;
                }*//*
            }
        });
    }*/

        buttonGo.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                if     ((validDate(new String(slot1.getText()))) &&
                        (validDate(new String(slot2.getText()))) &&
                        (validDate(new String(slot3.getText())))){
                    a = Double.parseDouble(slot1.getText());
                    b = Double.parseDouble(slot2.getText());
                    c = Double.parseDouble(slot3.getText());
                }else{
                    textBoxRez.setVisible(false);
                    labelError.setText("Введите данные еще раз");
                    labelError.setVisible(true);
                    return;
                }
                sendToServer();
                textBoxRez.setVisible(true);
                labelError.setVisible(false);
            }
        });
    }

    private void sendToServer(){

        quadraticService.rezultQuadratic(a, b, c, new AsyncCallback<Set<Double>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Server is not connecting");
            }

            @Override
            public void onSuccess(Set<Double> result) {
                if(result.isEmpty()){
                    textBoxRez.setText("Корней нет!");
                }else {
                    textBoxRez.setText(result.toString());
                    textBoxRez.setVisible(true);
                }
            }
        });
    }

    private boolean validDate(String a){
            if (!a.matches("[0-9]*")){
                return false;
            }
        return true;
    }
}