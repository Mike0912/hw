package client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.Set;

public interface QuadraticServiceIntfAsync {

    void rezultQuadratic(double a, double b, double c, AsyncCallback<Set<Double>> async) throws IllegalArgumentException;
}
